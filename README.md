# VCF To CLC

A lightweight application to convert between VCF and CLC.

## Usage

```vcftoclc <input-file> [output-file]```

`<>`: Required

`[]`: Optional
