﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace VcfToClc
{
	class Program
	{
		private static void Main(string[] args)
		{
			Console.WriteLine("VCF Converter - Copyright (c) 2020 Alexander Gokhale\n");
			
			if (args.Length == 0)
				DisplayUsage();

			if (!File.Exists(args[0]))
			{
				Console.WriteLine("Could not find a VCF file at the location specified.\n");
				Environment.Exit(2);
			}

			var lines = File.ReadAllLines(args[0]);
			var newLines = new List<string>();

			if (!lines[0].Contains("##fileformat=VCFv4.1"))
				DisplayInvalidFormat();

			foreach (var line in lines)
			{
				if (line.StartsWith('#'))
					newLines.Add(line);
				else
				{
					var lineParts = line.Split('\t');
					
					if (lineParts.Length != 10)
						DisplayInvalidFormat();

					var newLine = string.Empty;
					
					foreach (var linePart in lineParts)
					{
						if (lineParts.ToList().IndexOf(linePart) != 9)
							newLine += linePart + "\t";
						else
						{
							var sampleParts = linePart.Split(":");

							if (sampleParts.Length != 14)
								DisplayInvalidFormat();

							sampleParts[5] = $"{sampleParts[4]},{sampleParts[5]}";
							newLine += string.Join(':', sampleParts);
						}
					}
					
					newLines.Add(newLine);
				}
			}

			if (args.Length > 1)
			{
				try
				{
					File.WriteAllLines(args[1], newLines);
					Console.WriteLine($"File saved successfully to {args[1]}.\n");
				}
				catch
				{
					Console.WriteLine("Could not write the output file to the location specified\n");
					Environment.Exit(2);
				}
			}
			else
			{
				File.WriteAllLines("output.vcf", newLines);
				Console.WriteLine($"File saved successfully to {Path.GetFullPath("output.vcf")}.\n");
			}
		}

		private static void DisplayUsage()
		{
			Console.WriteLine(
				"To use this program you must specify a VCF file to convert,\n" +
				"and optionally where to save the converted file to. If this is\n" +
				"not specified, it will be saved into the same directory as this executable.\n");
			Console.WriteLine("Syntax: vcftoclc <input-file> <output-file>\n");
			
			Environment.Exit(1);
		}

		private static void DisplayInvalidFormat()
		{
			Console.WriteLine("The file provided was not in VCF v4.1 format\n");
			
			Environment.Exit(2);
		}
	}
}